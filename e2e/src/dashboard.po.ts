import { browser, element, by } from "protractor";

export class DashboardPage {

    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl + '/store')
    }

    async getLoginButton(): Promise<unknown> {
        return element(by.buttonText('Zaloguj')).getText();
    }

    async getDropdownButton(): Promise<unknown> {
        return element(by.id('dropdownManual')).click();
    }

    getDropDownButtonItem() {
        return element.all(by.css('dropdown-item')).count();
    }
}