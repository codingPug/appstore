import { count } from "console";
import { DashboardPage } from "./dashboard.po"

describe('dashboard is working properlu', () => {

    let dashboardPage: DashboardPage;

    beforeEach( ()=> {
        dashboardPage = new DashboardPage();
    })

    it('login button is visible', async () => {
        await dashboardPage.navigateTo();
        expect(await dashboardPage.getLoginButton()).toEqual('Zaloguj');
    })

    it('check if dropdown works', async () => {
        await dashboardPage.navigateTo();
        await dashboardPage.getDropdownButton();
        expect(dashboardPage.getDropDownButtonItem()).toBeCloseTo(3);

    })

})

